from random import random
import matplotlib.pyplot as plt
import numpy as np

#Constantes
NTl = 1000       #numero inicial de thalios
tau = 3.053*60   #vida media del thalio en seg.
    
#los puntos a graficar
temps = []

#Determinando tiempo de decaimiento
for i in range(NTl):
    t = -tau*np.log(1-random()/np.log(2))
    temps.append(t)

#La grafica
(counts, bins, patches) = plt.hist(temps)
plt.xlabel("Tiempos")
plt.ylabel("Eventos")
plt.show()
