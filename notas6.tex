\documentclass[10pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{braket}
\usepackage{pgf}
\usepackage{tikz}
\usepackage{tikz-feynman}[compat=1.1.0]
\author{Física Nuclear y Subnuclear}
\title{Aplicaciones de la física nuclear y de partículas}
\begin{document}
\maketitle

	
\section*{Fisión nuclear}
	
Si rememoramos las primeras lecciones de este curso, al hablar de los inicios del estudio de la física nuclear y de partículas recordaran que empezamos con la dispersión de Rutherford de partículas cargadas. En la parte de detectores tratamos con partículas cargadas para reconocer la interacción de las partículas con la materia. En todo momento hemos usado la interacción coulombiana para detectar la forma y características del núcleo. ¿Qué pasa si hacemos los mismos estudios con neutrones? Con la energía suficiente pueden penetrar al núcleo evitando la barrera coulombiana. Suena a puro beneficio, pero hay riesgos.
	
Originalmente se considero el método para generar elementos más pesados (transuránicos), pero se notó que antes que formar isótopos más pesados provocaba que el núcleo decayera en dos núcleos hijos. Por ejemplo en el ${}^{235}U^{92}$, que tiene $A$ impar, con neutrones a temperaturas bajas ($T\approx 300K$, $kT\approx 1/40\ eV$) se obtiene
	
\begin{equation}
  {}^{235}U^{92} + n \rightarrow {}^{148}La^{57} + {}^{87}Br^{35} + n
\end{equation}
	
Para el ${}^{238}U^{92}$, que como pueden ver es un núcleo par, la dispersión de neutrones termales no produce fragmentación, en este caso se requieren energías alrededor de los $2\ MeV$. 
	
¿Cuánta energía libera el proceso? Podemos revisar la diferencia en energías de enlace, como vimos en el capítulo pasado

\begin{align*}
  \Delta E =& B.E.(235,92)-B.E.(148,57)-B.E.(87,35)\\
  =& 177519.25 keV = 177.51925 MeV,
\end{align*}

\noindent que nos da casi $200\ MeV$ por cada fragmentación, este proceso es llamado \emph{fisión nuclear}. Noten que es un valor alto para las escalas nucleares.
	
Esto se puede explicar cualitativamente con el modelo de la gota. Pensemos en el núcleo como una gota redonda que es colisionada por una gota que representa al neutrón. Como seguramente su experiencia jugando con el agua les dicta que al chocar las dos gotas se produce una gota más grande, pero si la gota neutrón va con energía suficiente la forma esférica de la gota será deformada, entrando en una oscilación que la estira. Si no es tanta la energía la gota vibrará un poco y se quedará como una gota de mayor tamaño (captura radiativa del neutrón), pero si la perturbación es grande, en una de estas elongaciones la gota compuesta se rompera en dos gotas más o menos de la mitad del tamaño de la primera, justo como en la fisión. Un esquema se puede ver en la figura \ref{fig:gotas}.
	
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.3\linewidth]{gota.png}
    \caption{Oscilaciones del núcleo tras ser colisionado por un neutrón de acuerdo al modelo de la gota. Imagen de Hullernuc con licencia \href{https://creativecommons.org/licenses/by-sa/3.0/deed.en}{CC-BY-SA 3.0}}
    \label{fig:gotas}
  \end{center}
\end{figure}
	
Veamos que nos puede decir cuantitativamente el mismo modelo de la gota. La perturbación (el choque de la gota/neutrón) provoca que el núcleo de radio $R$ se deforme en una elipsoide del mismo volumen con eje semi-mayor $a$ y eje semi-menor $b$, en términos de un pequeño factor de deformación $\epsilon$
	
\begin{align*}
  a =& R(1+\epsilon) \\
  b =& \frac{R}{(1+\epsilon)^{\frac{1}{2}}} 
\end{align*}
	
Esta parametrización asegura que el volumen de la gota se mantiene constante (recordemos que tratamos al núcleo como un líquido incompresible)
	
\begin{equation*}
  V=\frac{4}{3}\pi R^3 = \frac{4}{3}\pi ab^2
\end{equation*}
	
Revisando los términos de la fórmula para el modelo de la gota podemos ver que como el volumen se conserva el primer término ($a_1 A$) se queda igual pero el segundo y tercero ya no. Como la tensión supericial depende del área superficial de la gota, esa tensión no será la misma para una gota esférica que para una gota elipsoidal.
	
\begin{equation*}
  a_2 A^{\frac{2}{3}} \rightarrow a_2 A^{\frac{2}{3}} \left( 1+\frac{2}{3}\epsilon^2 \right)
\end{equation*}
	
Para el término coulombiano, como depende de un potencia con simetría esférica, al cambiar la forma el potencial de igual manera cambia
	
\begin{equation*}
  a_3 \frac{Z^2}{A^{\frac{1}{3}}} \rightarrow a_3 \frac{Z^2}{A^{\frac{1}{3}}} \left( 1-\frac{1}{5}\epsilon^2 \right)
\end{equation*}
	
El término de tensión superficial aumenta en valor, mientras que el coulombiano disminuye, se inicia un juego entre estos dos términos (los únicos dos que se ven afectados de los tres que dependen de la forma de la gota, lo otros dos no cambian) que determinará el cambio en la energía de ligadura
	
\begin{align*}
  \Delta =& B.E.(\text{elipsoide}) - B.E.(\text{esfera}) \\
  =& \frac{2}{5} \epsilon^2 a_2 A^{\frac{2}{3}} - \frac{1}{5}\epsilon^2 a_3 \frac{Z^2}{A^{\frac{1}{3}}} \\
  =& \frac{1}{5}\epsilon^2 A^{\frac{2}{3}} \left( 2a_2 - a_3\frac{Z^2}{A} \right)
\end{align*}
	
El núcleo esférico es estable si la diferencia de energías es positiva (recordemos que $a_2 \approx 16.8\ MeV$ y $a_3\approx 0.72\ Mev$), es decir
	
\begin{align*}
   \left( 2a_2 - a_3\frac{Z^2}{A} \right) \text{ mayor qué } &0\\
  \text{es decir, } \frac{Z^2}{A} \text{ menor que } &47
\end{align*}
	
Esta relación se cumple incluso para núcleos pesados, donde $Z<N$, lo que nos dice es que un núcleo deformado tiende a ser inestable. Bien, pero esta inestabilidad podría revertirse si el núcleo deja de vibrar y se vuelva esférico, aún asi puede que dos núcleos hijos también sean energéticamente más conveniente. Con el mismo modelo de la gota podemos ver la diferencia en energías de enlace, imaginemos un núcleo de $A$ y $Z$ par que decae a dos núcleos hijos idénticos (con $A/2$ y $Z/2$)
	
\begin{align*}
  \Delta =& B.E.(A,Z) - 2B.E.(\frac{A}{2},\frac{Z}{2})\\
  =& a_2 A^{\frac{2}{3}}\left( 1-2\left( \frac{1}{2} \right)^{\frac{2}{3}} \right) + a_3\frac{Z^2}{A^{\frac{1}{3}}} \left( 1-2\frac{(\frac{1}{2})^2}{(\frac{1}{2})^{\frac{1}{3}}} \right) \\
  \approx & 0.27 A^{\frac{2}{3}}\left( -16.5 + \frac{Z^2}{A} \right)\ MeV
\end{align*}
	
De aquí vemos que si $Z^2>16.5 A$ el cambio en la energía de enlace será positivo y los dos núcleos hijos serán más estables que el núcleo padre.
	
\subsection*{Reacción en cadena}
	
Ya sabemos que la fisión libera bastante energía (el ${}^{235}U^{92}$ libera $\sim 200\ MeV$), pero para que sea una fuente útil de energía además necesitamos que la libere de forma constante, por ello es útil la fisión para generar energía. Por lo regular los núcleos fisionados liberan neutrones con energía suficiente para activar otras fisiones, generando una reacción en cadena, el núcleo antes mencionado libera un promedio de $2.5$ neutrones por fisión.
	
Definimos la razón de neutrones producidos por etapas sucesivas de la fisión
	
\begin{equation*}
  k=\frac{\text{Número de neutrones producido en la etapa } n+1}{\text{Número de neutrones producidos en la etapa }n}
\end{equation*}
	
Distinguimos las posibilidades
\begin{enumerate}
\item $k<1$ es un proceso \emph{subcrítico}, la reacción no se mantiene y no es útil para producir energía
\item $k=1$ es un proceso \emph{crítico}, se puede tener una reacción sosntenida y constante, es lo mejor para tener energía
\item $k>1$ es un proceso supercrítico, la reacción en cadena es incontrolable y cada vez se produce más y más energía, una explosión. 
\end{enumerate}
	
El tiempo de vida media del ${}^{235}U^{92}$ es de $\sim 7\times 10^8 \text{ años}$, para el ${}^{238}U^{92}$ es de $\sim 5\times 10^9\text{ años}$, este último siendo el más común en la naturaleza, la razón de ${}^{235}U^{92}$ por ${}^{238}U^{92}$ en una muestra obtenida de una mina es $\sim 1:138$. Para poder usar el ${}^{235}U^{92}$ se debe enriquecer el obtenido de forma natural, para tener más núcleos radiactivos.
	
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{reactor.jpg}
    \caption{Reactor CROCUS, instalaciones nucleares del EPFL. Imagen de \href{https://commons.wikimedia.org/wiki/User:Rama}{Rama}, con licencia \href{https://creativecommons.org/licenses/by-sa/2.0/fr/deed.en}{CC-BY-SA 2.0 Francia}}
    \label{fig:reactor}
  \end{center}
\end{figure}
	
En la figura \ref{fig:reactor} se muestra un reactor de investigación, mirando a detalle se pueden distinguir cilíndros metálicos donde se encuentra el combustible nuclear, las varillas que están fuera son las barras de control, dentro de la olla que contiene todo esto se encuentra el material moderador.
	
Las barras de control suelen ser construidas de cadmio, conveniente material por su gran sección eficaz de absorción para neutrones, su labor es regular la cantidad de neutrones, si hay demasiados se introducen las barras para que absorban los neutrones excedentes y procurar tener $k=\text{ constante}$, si en cambio se llega a $k<1$ se pueden retirar algunas hasta tener el proceso más cercano al estable. Los cilíndros de mayor diámetro como ya les mencioné contienen el combustible, alrededor de las barras de combustible se encuentra el material moderador cuya función es moderar la velocidad de los neutrones producidos, así se mantienen en energías térmicas y puede iniciarse la fisión. Suele usarse agua pesada ($D_2O$) por ser barata y poco absorbente a los neutrones.
	
Todo este arreglo esta rodeado de agua, esta es la encargada de absorber la energía como calor, al punto de hervir, generando vapor que en otra sección hace mover una turbina que genera electricidad. Además el agua enfría al reactor y evita que se funda.
	
Veamos ahora cuánta energía se libera por gramo de ${}^{235}U^{92}$, suponiendo que es un gramo de ese puro isótopo (es un caso muy ideal, nunca está tan enriquecido), tendremos como cantidad de núcleos
	
\begin{equation*}
  N=(1\ gr)\frac{N_A}{A} = (1\ gr.)\frac{6.023\times 10^{23} \frac{1}{mol}}{235 \frac{gr.}{mol}} \approx 2.56\times 10^{21} 
\end{equation*}
	
Como ya hemos calculado por cada fisión de ${}^{235}U^{92}$ se liberan alrededor de $200\ MeV = 2\times 10^8 eV = 3.2\times 10^{-11}J$ (ya que $1eV = 1.6\times 10^{-19}J$), cada núcleo de ese isótopo puede fisionar, por lo tanto
	
\begin{align*}
  E &\approx (3.2\times 10^{-11}J)(2.56\times 10^{21}) \\
  &\approx 8.19\times 10^{10} J \\
  &\approx 1\times 10^{11} J = 1MWD 
\end{align*}

En comparación, una tonelad de carbón puede producir $0.36\ MWD$, más o menos.

\emph{Otro ejercicio:} La sonda espacial \emph{Galileo} fue lanzada en 1989. Su fuente de poder eran $11 kg$ de ${}^{238}Pu$, un residuo nuclear de las armas nucleares de plutonio. La energía eléctrica se genera termoeléctricamente por el calor producido de las $\alpha$'s de $5.59MeV$ que se emiten en cada decaimiento del material. La vida media del ${}^{238}Pu$ es de $87.7$ años.

\begin{itemize}
\item ¿Cuál era la actividad inicial del combustible?
\item ¿Qué potencia emite (en kiloWatts)?
\end{itemize}

\textbf{Respuesta:} Para obtener la actividad necesitamos calcular la constante de decaimiento $\lambda = ln(2)/t_{1/2}$.

\begin{align*}
  t_{1/2} =& 87.7 yrs \times (365 dias/yr) \times (24 horas/dia)\times (60 min/hora)\times (60 seg/min) \\
  =& 2.7657 \times 10^9 seg.\\
  \lambda =& \frac{ln(2)}{2.7657 \times 10^9 seg.} \\
  =& 2.5062\times 10^{10} \frac{1}{seg.}
\end{align*}

Teniendo la constante de decaimiento ahora sólo resta conocer a cantidad de núcleos en los $11 kg$ iniciales. Para eso necesitamos la densidad del ${}^{238}Pu$ a temperatura ambiente (supongamos se queda a esa temperatura, claro que en Júpiter no es esa misma densidad) de $19.8 gr/cm^3$. Entonces la cantidad de núcleos en los $11kg$

\begin{equation*}
  N_0 = \frac{N_A}{A}(11\times 10^3 gr.) = \frac{6.022\times 10^{23}\frac{1}{mol}}{238gr/mol}(11\times 10^3 gr.) = 2.7833 \times 10^{25} 
\end{equation*}

De esta forma la actividad inicial sería

\begin{equation*}
  N_0 \lambda= 2.7833 \times 10^{25} \times 2.5062\times 10^{10} \frac{1}{seg.} =   7.152\times 10^{15} Bq.
\end{equation*}

Para obtener la cantidad de energía partimos de que cada uno de esos núcleos de la actividad libera $5.59 MeV$, que corresponde a $5.59 \times 10^6 eV (1.6\times 10^{-19}J/eV) = 8.944\times 10^{-13}J $

\begin{equation*}
  E = 7.152 \times 10^{15} \frac{1}{seg} \times  8.944\times 10^{-13}J = 6396.7488 Joules/seg. = 6.396 kW
\end{equation*}

	
\section*{Fusión Nuclear}
	
Si ahora en lugar de dividir un núcleo pesado en dos núcleos más ligeros para llegar a la partre más estbale del espectro nuclear ($A=60$) juntamos dos núcleos ligeros para formar uno más estable, también liberamos energía, con la conveniencia que los núcleos ligeros son más abundantes que  los núcleos pesados. El proceso es llamado fusión, y a pesar de no liberar las mismas cantidades de energía que la fisión (hay menos nucleones inmiscuidos) es una fuente tan buena de energía que es el proceso predominante en nuestra principal fuente de energía natural: el Sol.
	
Al tratar de fusionar dos núcleos ligeros la primera barrera que encontraremos es la repulsión coulombiana entre ellos, cuando los núcleos se están tocando esta repulsión llega al máximo
	
\begin{align*}
  V_{Coulomb} &= \frac{ZZ'e^2}{R+R'} \\
  &= \frac{e^2}{\hbar c} \frac{\hbar c Z Z'}{1.2[A^{\frac{1}{3}}+{A'}^{\frac{1}{3}}]fm}\\
  &= \frac{1}{137} \left( \frac{197 MeV-fm}{1.2 fm} \right) \frac{ZZ'}{A^{\frac{1}{3}}+{A'}^{\frac{1}{3}}} \\
  &\approx \frac{ZZ'}{{A^{\frac{1}{3}}+{A'}^{\frac{1}{3}}}} MeV \approx \frac{1}{8} A^{\frac{5}{3} MeV}
\end{align*}
	
Usando la aproximación $A\approx A'$, $A\approx 2Z$, donde los valores sin primar son para un núcleo y los primados para otro. Así, por ejemplo, si queremos fusionar dos núcleos con $A=8$ la energía necesaria sería alrededor de $4\ MeV$ para romper la barrera coulombiana.
	
Colisionar haces de núcleos ligeros es un proceso poco efectivo, la mayor parte de los núcleos sufrirán dispersiones elásticas, una mejor opción es calentarlos para que los núcleos alcancen energías cinéticas del orden de $2\ MeV$ por núcleo (tomando en cuenta el ejemplo anterior), sabiendo que a temperatura ambiente tiene la equivalencia $300K\approx 1/40\ eV$, $2\ MeV$ corresponden a 
	
\begin{equation*}
  \frac{2\times 10^6 eV}{\frac{1}{40}eV}\times 300K \approx 10^{10} K
\end{equation*}
	
La temperatura promedio del Sol es $\approx 10^7 K$, bastante por debajo de los $2\ MeV$, pero aún así la distribución alcanza el valor listado (la cola Maxwelliana). Dos de los procesos que suceden en las estrellas los explicaremos a continuación.
	
El Sol tiene una masa aproximada de $10^{30} kg$, suponiendo que es puro hidrógeno (como lo fue en un principio) contendría alrededor de $10^{56}$ átomos del elemento, lo que nos daría la idea de que la fusión de hidrógeno puede ser la fuente principal de energía, el ciclo $p-p$, sugerido inicialmente por Bethe
	
\begin{align*}
  {}^1H^1 + {}^1H^1 &\rightarrow {}^2H^1 + e^+ + \nu_e + 0.42MeV, \\
  {}^1H^1 + {}^2H^1 &\rightarrow {}^3He^2 + \gamma + 5.49MeV, \\
  {}^3He^2 + {}^3He^2 &\rightarrow {}^4He^2 + 2({}^1H^1) + 12.86MeV.
\end{align*}	
	
Como el ${}^4He^2$ es doble mágico al llegar a él se libera mucha energía, se llega a un sub-pico de estabilidad. Si contamos todos los protones necesarios para el ciclo tenemos
	
\begin{align*}
  6({}^1H^1) &\rightarrow {}^4He^2 + 2({}^1H^1) + 2e^+ + 2\nu_e + 2\gamma + 24.68MeV \\
  4({}^1H^1) &\rightarrow {}^4He^2 + 2e^+ + 2\nu_e + 2\gamma + 24.68MeV
\end{align*}
	
Los positrones salientes pueden aniquilarse con los electrones en el medio (a tales temperaturas el plasma solar está altamente ionizado), cooperando a liberar más energía. De igual forma los fotones pueden interactuar con el medio interestelar y liberar más energía.
	
Dado que la edad del universo es $\sim 10^{10}$ años, se estima que al Sol le quedan $10^9$ años de combustible.
	
El segundo ciclo que mencionaremos que sucede dentro de las estrellas es el ciclo del carbono, o ciclo CNO. Del helio al que llegamos en el ciclo anterior se puede llegar a núcleos de carbono a través de la reacción
	
\begin{equation*}
  3({}^4He^2) \rightarrow {}^{12}C^6 + 7.27MeV
\end{equation*}
	
Ese nucleo de ${}^{12}C^6$ puede absorber un protón (recordemos que el hidrógeno es lo más abundante en la estrella) y se llega al ciclo
	
\begin{align*}
  {}^{12}C^6 + {}^1H^1 &\rightarrow {}^{13}N^7 + \gamma + 1.95MeV \\
  {}^{13}N &\rightarrow {}^{13}C^6 + e^+ + \nu_e + 1.20MeV \\
  {}^{13}C^6 + {}^1H^1 &\rightarrow {}^{14}N^7 + \gamma + 7.55MeV \\
  {}^{14}N^7 + {}^1H^1 &\rightarrow {}^{15}O^8 + \gamma + 7.34MeV \\
  {}^{15}O^8 &\rightarrow {}^{15}N^7 + e^+ + \nu_e + 1.68MeV \\
  {}^{15}N^7 + {}^1H^1 &\rightarrow {}^{12}C^6 + {}^4He^2 + 4.96MeV 
\end{align*}
	
En el ciclo completo tenemos efectivamente
	
\begin{align*}
  {}^{12}C^6 +4({}^1H^1) &\rightarrow {}^{12}C^6 + {}^4He^2 + 2e^+ + 2\nu_e + 3\gamma + 24.68MeV \\
  4({}^1H^1) &\rightarrow {}^4He^2 + 2e^+ + 2\nu_e + 3\gamma + 24.68MeV
\end{align*}
	
Es de gran interés lograr la fusión controlada como fuente de energía en la Tierra, al momento de edición del Ferbel las reacciones observadas en condiciones de laboratorio son
	
\begin{align*}
  {}^2H^1 + {}^3H^1 &\rightarrow {}^4He^2 + n + 17.6MeV \\
  {}^2H^1 + {}^2 H^1 &\rightarrow {}^3He^2 + n + 3.2MeV \\
  {}^2H^1 + {}^2H^1 &\rightarrow {}^3H^1 + {}^1H^1 + 4.0MeV
\end{align*}

\section*{Radiactividad natural y datación radiométrica}
	
Hay alrededor de 60 núcleos radiactivos que se encuentran en la naturaleza, en comparación a los 1000 núcleos radiactivos creados artificialmente son una cantidad pequeña. Hay núcleos que a lo largo de la edad del universo ya han decaído, a partir de esta información podemos obtener medidas de tiempo de distintas cosas en nuestro planeta.
	
La mayor parte de los núcleos radiactivos hallados en la naturaleza son muy pesados, están alrededor de $81\leq Z \leq 92$, al ser tan pesados, como vimos en la gráfica de las islas de estabilidad, tendrán varios procesos radiativos hasta llegar a la estabilidad, por ejemplo, empezando por un decaimiento alfa reducirán en cuatro unidades su $A$. Dado que son tan pesados también sabemos que tendrán un exceso de neutrones, al bajar en 4 unidades su $A$, pero en dos unidades su $Z$ y $N=A-Z$, aún se mantendrá el exceso de neutrones, siendo incluso más notable en núcleos más ligeros, puede ahora reducir ese exceso por decaimieno $beta^-$, el nuevo núcleo puede decaer por $\alpha$ o $\beta$ consecutivamente hasta llegar a la estabilidad.
	
La combinación de decaimientos generará series de núcleos que mantendrán una relación con el número de sus nucleones, dando como resultado las cuatro series de emisores $\alpha$ pesados cuyos hijos difieren progresivamente por cuatro nucleones en su valor de $A$
	
\begin{itemize}
\item $A=4n$ serie del Torio,
\item $A=4n+1$ serie del Neptunio,
\item $A=4n+2$ serie del Uranio-Radio,
\item $A=4n+3$ serie del Uranio-Actinio,
\end{itemize}
	
\noindent con $n$ un valor entero. Las correcciones en los nombres se debe a que históricamente se identificaron las dos últimas series con el $Ra$ y el $Ac$ aunque realmente el primer caso proviene del ${}^{238}U^{92}$ y el último del ${}^{235}U^{92}$. La vida media de estos núcleos padres
	
\begin{itemize}
\item $t_{\frac{1}{2}}({}^{232}Th^{90})= 9.63\times 10^9$ años
\item $t_{\frac{1}{2}}({}^{237}Np^{93})= 1.5\times 10^6$ años
\item $t_{\frac{1}{2}}({}^{238}U^{92})= 3.12\times 10^9$ años
\item $t_{\frac{1}{2}}({}^{235}U^{92})= 4.96\times 10^8$ años
\end{itemize}
	
Dado que el Neptunio es el de menor vida media, para esta edad del universo ya no podríamos ver ninguno de los isótopos radiactivos en esa serie, y así es, sólo de las otras tres series aún hay evidencia sobre sus núcleos padres. La estabilidad de las tres series restantes las define el Pb, en particular ${}^{208}Pb^{82}$ para el Th, ${}^{206}Pb^{82}$ para el ${}^{238}U^{92}$ y ${}^{207}Pb^{82}$ para el ${}^{235}U^92$. 
	
Uno de estos núcleos radiactivos naturales que es útil para determinar la edad de un material orgánico es el ${}^{14}C^6$, es útil para edades en el órden de miles de años. El principio es que en la atmósfera el ${}^{12}C^6$ y el ${}^{14}N^7$ son núcleos muy abundantes, viajan en el aire mismo que es bombardeado por rayos cósmicos constantemente. En estos proceso de choques de rayos cósmicos pueden generarse neutrones lentos que al impactar con ${}^{14}N^7$ sucede la interacción
	
\begin{equation*}
  {}^{14}N^7 + n \rightarrow {}^{14}C^6 + p
\end{equation*}	 
	
El ${}^{14}C^6$ decae con un tiempo de vida media de 5730 años a través de emisiones $\beta^-$
	
\begin{equation*}
  {}^{14}C^6 \rightarrow {}^{14}N^7 + e^- + \bar{\nu_e}
\end{equation*}
	
Posterior a esta interacción la atmósfera contiene una pequeña porción de ${}^{14}C^6$ y grandes porciones de  ${}^{12}C^6$, ambos isótopos pueden combinarse con el oxígeno y convertirse en $CO_2$. Los organismos vivos, como las plantas, consumen $CO_2$, mientras el organismo vive constantemente está absorbiendo la molécula, sea la que contiene al isótopo estable o la que contiene al isótopo radiactivo, al momento de morir este intercambio cesa y se queda con la cantidad de $CO_2$ de su última absorción, el ${}^{14}C^6$ contenido en algunas moléculas empieza a decaer, mientras que el ${}^{12}C^6$ del restante se mantiene. Midiendo la cantidad de ${}^{14}C^6$ con respecto al ${}^{12}C^6$ podemos saber la edad del material orgánico, o midiendo la actividad y comparándola con la de un organismo aún vivo. Esta es la datación por ${}^{14}C^6$, por los rangos de tiempo es muy útil en estudios arqueológicos.
	
Veamos un ejemplo: Una pieza de madera de $50gr$ tiene una actividad de $320$ desintegraciones por minuto, lo que es equivalente a $5.33Bq$, la actividad de una planta viva es de $12$ desintegraciones/minuto/gramos ($0.2Bq$ por gramo) ¿Qué edad tiene el pedazo de madera? Sabemos que la vida media del ${}^{14}C^6$ es de 5730 años, entonces
	
\begin{equation*}
  \lambda = \frac{ln(2)}{t_{\frac{1}{2}}} = \frac{ln(2)}{1.8\times 10^{11} seg} = 3.83\times 10^{-12} \frac{1}{seg.}
\end{equation*}
	
Tenemos la actividad inicial, cuando el pedazo de madera pertenecía a un árbol vivo
	
\begin{equation*}
  \mathcal{A}_0 = 0.2Bq/gr \times 50gr. = 10 Bq
\end{equation*}
	
La actividad actual es de $5.34Bq$ (chequen que siempre debe ser menor la actividad la paso de los años), lo que sabemos de nuestra ley de decaimiento es que
	
\begin{align*}
  \mathcal{A}(t) =& \mathcal{A}_0 e^{-\lambda t}\\
  \text{reacomodando } \frac{\mathcal{A}(t)}{\mathcal{A}_0} =& e^{-\lambda t}\\
  \text{despejando } t=& \frac{1}{\lambda}ln\left( \frac{\mathcal{A}_0}{\mathcal{A}(t)} \right)
\end{align*}
	
Sustituimos los valores que tenemos
	
\begin{align*}
  t &= \frac{1}{\lambda}ln\left( \frac{\mathcal{A}_0}{\mathcal{A}(t)} \right) \approx \frac{1}{3.83\times 10^{-12} \frac{1}{seg.}}ln\left( \frac{10Bq}{5.34Bq} \right) \\ 
  &\approx 1.64 \times 10^{11} seg \approx 5194\text{ años}
\end{align*}
	
La pieza de madera tienen alrededor de 5194 años, debe ser un fósil.
	
Con el uso del espectrógrafo de masas se logra hacer este proceso mejor hoy en día, antes se necesitaba una muestra de $1gr$ para hacer la prueba, ahora basta con $1mgr$, con una sensitividad de $10^{-14}$ en la razón de ${}^{14}C^6/{}^{12}C^6$
	
\section*{Dosimetría}
Todos sabemos que estas radiaciones de las que hemos hablado son peligrosas, pueden dañar nuestra salud y la de los seres vivos alrededor. No tenemos el espacio y el tiempo para dar un curso completo de seguridad radiológica pero es útil tengan nociones básicas por si en algún momento entran a uno de estos laboratorios y reciben un curso exprés de seguridad radiológica, o si terminan dedicandose a la física de radiaciones en hospitales o la industria.
	
El daño medible que estas radiaciones pueden producir es debido a la ionización, así las unidades serán una medida de la ionización o de la energía depositada en el material.
	
La unidad más antigua es el Roentgen, que es una unidad de exposición
	
\begin{align*}
  1\text{ Roentgen} =& \text{ la cantidad de rayos X que producen una ionización de } 1\ esu/cm^3 \\
  =& 2.58 coul./kg \text{ para aire en STP}
\end{align*}
	
Pero esta unidad trata sólo de rayos X o $\gamma$ absorbidos en el aire, no se ocupa de otras radiaciones ni de material vivo. Ioniza el aire en su mayoría debido a los electrones derivados de la radiación $\gamma$ (por efecto Compton y fotoeléctrico), entonces es una medida mixta, si queremos construirla a partir de magnitudes accesibles asumimos una radiación isotrópica de un punto e ignoramos la atenuación en el aire, la ionización por unidad de tiempo o razón de exposición para una fuente dada se puede encontrar con
	
\begin{equation*}
  \text{Razón de exposición } = \frac{\Gamma \mathcal{A}}{d^2},
\end{equation*}
	
\noindent donde $\mathcal{A}$ es la actividad, $d$ la distancia a la fuente y $\Gamma$ la constante de razón de exposición que depende de los esquemas de decaimiento de los procesos en particular, la energía de los $\gamma$'s, el coeficiente de absorción en aire y la ionización especifica de los electrones. En la tabla \ref{tab:razon} se pueden ver algunos valores para la constante.
	
\begin{table}[ht!]
  \begin{tabular}{|p{0.3\textwidth} p{0.3\textwidth}|}
    \hline
    Fuente & $\Gamma [R\cdot cm^2/(hr\cdot mCi)]$   \\
    \hline
    ${}^{137}Ce$ & 3.3   \\
    ${}^{57}Co$ & 13.2  \\
    ${}^{22}Na$ & 12.0  \\
    ${}^{60}Co$ & 13.2 \\
    ${}^{222}Ra$ & 8.25 \\
    \hline 
  \end{tabular}
  \caption{Tabla de valores de $\Gamma$ para distintos núcleos.}
  \label{tab:razon}
\end{table}
	
\subsection*{Dosis absorbida}
Una medida más útil para discutir los efectos de la radiación es la \emph{dosis absorbida}, mide el total de energía absorbida por unidad de masa, las dos unidades que la miden
	
\begin{align*}
  1rad &= 100erg/gr \\
  1Gray &= 1 Joule/kg = 100rad 
\end{align*}			
	
Siendo la segunda más actual. No se diferencia entre fuentes ni la razón con la que la irradiación ocurre.
	
Veamos un ejemplo: Calcula la dosis absorbida en el aire para 1 Roentgen de rayos $\gamma$. Asume que para electrones, la energía promedio necesaria para producir un par ión-electrón es de $32eV$.
	
Tenemos que 
	
\begin{equation}
  1 R = 1 esu/cm^3= \frac{1}{3.33\times 10^{-10}coul/esu}
\end{equation}
	
La cantidad que salga de esa evalución serán pares ión-electrón/$cm^3$, para calcular energía necesitamos saber la energía que se necesita en el aire para producir un par ión-electrón y la densidad volumétrica del aire
	
\begin{equation*}
  \text{dosis absorbida} = \frac{1}{3.33\times 10^{-10}coul/esu} \times 32eV/\text{ión-electrón} \times \frac{1}{\rho_V}
\end{equation*}
	
Que deben convertir a las unidades necesarias para tener rads o Grays, como ustedes prefieran (eso se los dejo para la tarea).
	
\subsection*{Efectividad biológica relativa}
	
Hasta aquí aún no tenemos una unidad que caracterice el daño de acuerdo a la radiación, no es lo mismo ser irradiado por electrones que por alfas, los daños son distintos. Esta diferencia radica en la transferencia de energía lineal (LET) de las distintas partículas (la energía depositada localmente por unidad de longitud, como $dE/dx$). Mientras más ionizante sea la partícula más será el daño biológico que dejará localmente. Para dar cuenta de este efecto se define un factor de calidad, la efectividad biológica relativa (RBE) que tiene un valor distinto para cada tipo de radiación. Si no conocemos el espectro de energías de las radiaciones se utilizan los valores listados en la tabla \ref{tab:rbe}. Estos valores pueden variar de acuerdo al daño biológico específico
	
\begin{table}[ht!]
  \begin{tabular}{|p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth} p{0.1\textwidth}|}
    \hline
    & $\gamma$ & $\beta$ & $p$ & $\alpha$ & $n$ rap. & $n$ term.   \\
    \hline
    RBE & $1$ & $1$ & $10$ & $20$ & $10$ & $3$  \\
    
    \hline 
  \end{tabular}
\end{table}
\label{tab:rbe}
	
\subsection*{Dosis equivalente}
	
Si multiplicamos el factor RBE por la dosis absorbida estamos agregando el factor de daño biológico, lo que nos da una mejor idea del daño que puede producir la radiación. Sus unidades son el Roentgen-equivalente-persona (rem) o el Sievert (Sv)
	
\begin{align*}
  \text{rem} &= \text{RBE} \times rad \\
  \text{Sievert}(Sv) &= \text{RBE} \times Gray\ (1Sv=100rem)
\end{align*}
	
Esta unidad es más usada, aunque no es directamente medible, como sí lo es la dosis absorbida.
	
Veamos las dosis quivalentes de algunas fuetes de radiación natural
	
\begin{table}[ht!]
  \begin{tabular}{|p{0.4\textwidth} p{0.4\textwidth} |}
    \hline
    Fuentes naturales &    \\
    \hline
    Rayos cósmicos & $28mrem/\text{año}$  \\
    Fondo natural (U, Th, Ra) & $26mrem/\text{año}$  \\
    Fuentes radiactivas dentro del cuerpo (${}^{40}K$, ${}^{14}C$) & $26mrem/\text{año}$  \\
    \hline 
    Fuentes ambientales & \\
    \hline
    Debidas a la tecnología & $4mrem/\text{año}$ \\
    Contaminación radiactiva global & $4mrem/\text{año}$ \\
    Energía nuclear & $0.3mrem/\text{año}$ \\
    \hline
    Fuentes médicas & \\
    \hline
    Diagnostico & $78mrem/\text{año}$ \\
    Rayos X & $100-200mrem/\text{año}$ \\
    Fármacos & $14mrem/\text{año}$ \\
    Ocupacional & $1mrem/\text{año}$ \\
    Productos (TV) & $5mrem/\text{año}$ \\
    \hline
  \end{tabular}
\end{table}
	
\section{Física nuclear y de partículas en la astrofísica}
	
Estas dos áreas han ampliado el espectro de observación de la astronomía más allá del espectro visible y de los fenómenos clásicos. Ahora se pueden entender diversos procesos dentro de las estrellas, veamos un poco.
	
En 1929 Hubble descubrió un corrimiento al rojo en el espectro lumínico de las estrellas en galaxias lejanas, esto indica que el universo se expande a una velocidad
	
\begin{equation*}
  v=H_0 r,
\end{equation*}
	
\noindent donde $H_0\sim 70 km/s/Mparsec$ es la constante de Hubble ($1Mparsec=3.09\times 10^{19}km$). Se he observado que no es exactamente una constante.
	
Gamow propuso que en su etapa inicial el universo era una densa bola de neutrones a alta temperatura. El modelo actual difiere mucho de este pero se mantiene la alta densidad y las altas temperaturas. Aún es un problema saber como se paso de ese estado incial al universo hoy conocido, pensarlo como una singularidad aún atrae el problema de no tener una teoría cuántica de la gravedad. El universo tine una edad alrededor de los $14$ billones de años, en ese tiempo ha pasado por fases muy bien definidas.
	
\begin{table}[ht!]
  \begin{tabular}{|p{0.18\textwidth} p{0.15\textwidth} p{0.15\textwidth} p{0.18\textwidth} p{0.18\textwidth}|}
    \hline
    Edad & Temperatura (K) & Energia (eV) & Transición & Era   \\
    \hline
    $1.4\times 10^{10}$ años & $2.7$ & $\sim 10^{-4}$ &  & Epoca actual, estrellas \\
    \hline
    $4\times 10^{5}$ años & $3\times 10^3$ & $\sim 10^{-1}$ & Plasma a átomos & Fotón \\
    \hline
    3 minutos & $10^9$ & $\sim 10^{5}$ & Nucleosíntesis & Particulas \\
    \hline
    $10^{-6}$ seg. & $10^{12}$ & $\sim 10^8$ & Cuarks (hadronización) & Cuark \\
    \hline
    $10^{-10}$ seg. & $10^{15}$ & $\sim 10^{11}$ & Unificación electrodébil & Electrodébil \\
    \hline
    $10^{-33}$ seg. & $10^{28}$¿? & $\sim 10^{24}$ & Inflación & Inflación \\
    \hline
    $10^{-43}$ seg. & $10^{32}$ & $\sim 10^{28}$ & Todas las fuerzas unificadas & SUSY, Planck \\
    \hline
    0 &  &  & Vacío a materia &  \\
    \hline 
  \end{tabular}
\end{table}
	 		
\begin{thebibliography}{10}
\bibitem{DasFerbel}Das, A., Ferbel, T. ``Introduction to Nuclear and Particle Physics'', Segunda edición, World Scientific Publishing Co., 2003.
		
\bibitem{Cohen}Cohen, Bernard L. ``Concepts of Nuclear Physics'', McGraw-Hill, 1971.
		
\bibitem{Leo}Leo, William R. ``Techniques for Nuclear and Particle Physics Experiments'', Springer Verlag, 1987.

\bibitem{Henley}Henley, Ernest M., García, Alejandro ``Subatomic Physics'', Tercera edición, World Scientific Publishing Co., 2007.

\end{thebibliography}	
		 				 	 
\end{document}
