Notas para el curso de física nuclear y subnuclear, grupo 8465, de la facultad de ciencias UNAM.
Para más información del curso se puede consultar (página del semestre 2024-1)
[http://marcovladimir.codeberg.page](http://marcovladimir.codeberg.page)

Las notas se van actualizando, cualquier comentario es bien recibido por el correo: vladimir@ciencias.unam.mx, o por XMPP: vladimir@suchat.org.

