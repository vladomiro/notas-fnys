\documentclass[10pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{braket}
\usepackage{pgf}
\usepackage{tikz}
\usepackage{tikz-feynman}[compat=1.1.0]
\author{Física Nuclear y Subnuclear}
\title{Física Nuclear II Radiación Nuclear}
\begin{document}
\maketitle
\section*{Introducción}
	
En este momento sabemos ya algunas cosas y hemos hablado de otras:
	
\begin{itemize}
\item Los núcleos están compuestos por protones y neutrones
\item Protones y neutrones sienten las fuerzas: electromagnética, nuclear fuerte y nuclear débil
\item Núcleos de helio, electrones y fotones los hemos tratado, pero no hemos hablado más de ellos como radiación
\end{itemize}
	
\section*{Decaimiento alfa}
	
El decaimiento alfa representa la desintegración de un núcleo padre a otro hijo, por medio de la emisión de un núcleo de ${}^4He^2$. Se puede representar de la forma
	
\begin{equation*}
  {}^AX^Z \rightarrow {}^{A-4}Y^{Z-2} + {}^4He^2
\end{equation*}	 
	
Esto es similar a una fisión nuclear, aunque con dos núcleos hijos de masas muy dispares. Suponiendo que el núcleo inicial está en reposo podemos considerar por conservación de energía
	
\begin{equation*}
  M_P c^2 = M_Hc^2 + T_H + M_{\alpha}c^2 + T_{\alpha},
\end{equation*}
	
\noindent donde $M_P$ es la masa del núcleo padre, $M_H$ la masa del núcleo hijo, $M_{\alpha}$ la masa del núcleo de ${}^4He^2$, $T_H$ la energía cinética del núcleo hijo y $T_{\alpha}$ ya se imaginan. La ecuación la acomodamos para que queden energías de un lado, masas del otro
	
\begin{equation}
  T_H + T_{\alpha} = (M(A,Z) - M(A-4,Z-2) - M(4,2))c^2
  \label{ec:qq}
\end{equation}
		
Tomemos una aproximación clásica, sabemos que debe conservarse la energía y el momento, es lo que se puede leer en las ecuaciones pasadas, en particular podemos escribir la energía cinética del núcleo hijo y de la partícula $\alpha$
	
\begin{align}
  T_H =& \frac{1}{2} M_H v_H^2, \notag \\
  T_{\alpha} =& \frac{1}{2} M_{\alpha} v_{\alpha}^2,
  \label{ec:cin}
\end{align}			
	
\noindent donde hemos etiquetado de manera similar sus velocidades no relativistas. También sabemos que el momento debe conservarse, pero como el núcleo padre estaba en reposo los núcleos hijos deben tener momentos que al sumarse vectorialmente se anulen, es decir, los momentos tienen sentidos contrarios pero misma magnitud.
	
\begin{align}
  M_H v_H =& M_{\alpha} v_{\alpha}, \notag \\
  \text{despejando, } v_H =& \frac{M_{\alpha}}{M_H} v_{\alpha}
  \label{ec:vel}
\end{align}
		
Analizando el término vemos que cuando la masa del núcleo hijo es mayor que la de la partícula alfa (lo que sucede la mayoría de las veces), $M_H \gg M_{\alpha}$, entonces la razón se vuelve menor que uno y la velocidad del núcleo hijo es mucho menor que la de la partícula alfa, $v_H\ll v_{\alpha}$. Por lo tanto la energía cinética del núcleo hijo es mínima, reescribimos la ecuación \ref{ec:qq} con las energías dadas en \ref{ec:cin}, sustituyendo la velocidad del núcleo hijo de la ecuación \ref{ec:vel}
	
\begin{align*}
  T_H + T_{\alpha} =& \frac{1}{2}M_H \left( \frac{M_{\alpha}}{M_H} v_{\alpha} \right)^2 + \frac{1}{2} M_{\alpha} v_{\alpha}^2 \\
  =& \frac{1}{2} M_{\alpha} v_{\alpha}^2 \left( \frac{M_{\alpha}}{M_H} +1 \right) \\
  =& T_{\alpha}\frac{M_{\alpha} + M_H}{M_H}
\end{align*}
	
Tomando en cuenta todas estás consideraciones, podemos echar un ojo a la ecuación \ref{ec:qq} y ver que el proceso de decaimiento libera energía, pasa de un núcleo pesado a dos más ligeros. Para los núcleos más pesados (para los cuales sucede la mayoría de veces este proceso) la mayor parte de la energía se la lleva la partícula alfa, el núcleo hijo sólo se lleva de energía
	
\begin{align*}
  T_H &= T_{\alpha}\left( \frac{M_{\alpha} + M_H}{M_H}\right) - T_{\alpha} \\
  &= T_{\alpha}\left( \frac{M_{\alpha} + M_H}{M_H} - 1\right) \\
  &= T_{\alpha} \frac{M_{\alpha} + M_H - M_H}{M_H} = \frac{M_{\alpha}}{M_H}T_{\alpha}\ll T_{\alpha}
\end{align*}
	
Midiendo la energía de la partícula alfa en el decaimiento podríamos saber la energía liberada con una buena precisión, pero experimentalmente se han observado diversas energías, si bien se da el caso de que el núcleo padre decaiga al estado base del núcleo hijo y la partícula alfa, liberando la energía como hemos visto, hay casos en que se decae a un estado excitado del núcleo hijo, liberando menos energía e incluso liberando fotones posteriormente
	
\begin{equation*}
  {}^AX^Z \rightarrow {}^{A-4}{Y^{*}}^{Z-2} + {}^4He^2
\end{equation*}	
	
\noindent donde	${}^{A-4}{Y^{*}}^{Z-2}$ es el núcleo excitado (por eso el asterísco), que eventualmente decae al estado base liberando un fotón
	
\begin{equation*}
  {}^{A-4}{Y^{*}}^{Z-2} \rightarrow {}^{A-4}Y^{Z-2} + \gamma
\end{equation*}
	
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{excitados.jpg}
    \caption{Decaimineto por emisión $\alpha$ del ${}^{228}Th^{90}$ al ${}^{224}Ra^{88}$. Imagen tomada de \cite{DasFerbel}.}
    \label{fig:excitados}
  \end{center}
\end{figure}
	
Veamos un ejemplo para obtener un aproximado de la energía liberada por un decaimiento alfa (a estados base).
	
\begin{equation*}
  {}^{240}Pu^{94} \rightarrow {}^{236}U^{92} + {}^4He^2
\end{equation*}			 		
	
Utilizamos la ecuación \ref{ec:qq} para saber cuanta energía libera (que se traducirá a energía cinética del núcleo hijo y la partícula alfa)
	
\begin{align*}
  (M(240,94) - M(236,92) - M(4,2))c^2 &= E \\
  94m_p + 146m_n +B.E.(240,94) -92m_p-144m_n - &B.E.(236,92) \\ 
  - 2m_p -2m_n -B.E.(4,2) &= E \\
  B.E.(240,94) - B.E.(236,92) - B.E.(4,2) &= E \\
  -1813.4501\ MeV + 1790.4103\ MeV + 28.2956 &\approx 5.2558\ MeV 
\end{align*}
	
\subsection*{Penetración de barrera}
	
La altura e la barrera coulombiana para un núcleo pesado ($A\approx 200$) está en el rango de los $20-25\ MeV$, como ya vimos una partícula $\alpha$ sale con una energía cercana a los $5\ MeV$ ¿cómo es que el núcleo puede liberar una partícula $\alpha$ entonces? La emisión de la partícula $\alpha$ es un fenómeno de mecánica cuántica.
	
Para entender el proceso podemos pensar que la partícula $\alpha$ está atrapada en el mismo pozo de potencial del núcleo hijo, esto es derivado del trabajo de George Gamow, Ronald Gurney y Edward Condor. Tomemos el decaimiento
	
\begin{equation*}
  {}^{232}Th^{90} \rightarrow {}^{228}Ra^{88} + {}^4He^2
\end{equation*}	 
	
Experimentalmente se ve que la energía de la partícula $\alpha$ es de $4.05\ MeV$ (se puede checar la energía liberada y ver que la mayor parte se la lleva como energía cinética la partícula $\alpha$), la vida media del ${}^{232}Th^{90}$ es de $\tau = 1.39\times 10^{10}\ \text{años}$, su radio $R=r_0(232)^{1/3} fm. \approx 7.37 \times 10^{-15}m.$. Para que la partícula $\alpha$ escape debe penetrar la barrera coulombiana, aunque el caso real es en tres dimensiones esto dificulta mucho los cálculos, quedarnos con la aproximación a 1 dimensión es bueno, lo que nos interesa es tener un estimado en orden de magnitud.
	
Sustituiremos la barrera coulombiana por una barrera rectangular de la misma área, eligiendo $V_0 > E$ (la raíz $\sqrt{V_0-E}$ estará en los números reales), podemos elegir
	
\begin{align*}
  V_0 =& 14\ MeV \\
  2a =& 33\times 10^{-15}m,
\end{align*}	 
	
\noindent donde $2a$ es la longitud de la barrera de potencial. De estas consideraciones se obtiene un coeficiente de transmisión
	
\begin{align*}
  T =& \frac{\frac{4k_1k}{(k_1+k)^2}}{1+\left[ 1 + \left( \frac{\kappa^2 - k_1k}{\kappa(k_1+k)}\right)^2 \right]} \\
  \text{con } k_1 =& \left[ \frac{2M_{\alpha}}{\hbar^2} (E+U_0) \right]^{\frac{1}{2}} \\
  k =& \left[ \frac{2M_{\alpha}}{\hbar^2} E \right]^{\frac{1}{2}} \\		
  \kappa =& \left[ \frac{2M_{\alpha}}{\hbar^2} (V_0 - E) \right]^{\frac{1}{2}} 
\end{align*}
	
Con $E$ la energía cinética de la partícula alfa, $M_{\alpha}$ su masa. Eligiendo los valores correspondientes ($M_{\alpha}c^2 \approx 4000\ MeV$, $E=4.05\ MeV$, $V_0=14\ MeV$ y $U_0 \approx 40\ MeV$) podemos obtener una aprocimación para el coeficiente de transmisión desde afuera hacia adentro
	
\begin{equation*}
  T\approx 4\times 10^{-40}
\end{equation*}
	
La probabilidad de que una partícula $\alpha$ penetre la barrera es bajísima, por ello los núcleos de ${}^4He^2$ no son reabsorbidos tras el decaimiento alfa.
	
En cambio desde adentro la situación cambia, la probabilidad de que la partícula alfa escape, por segundo, es
	
\begin{align*}
  P(\text{emisión }\alpha) &\approx \frac{v_{\alpha}}{R}T \approx 6\times 10^{21}\frac{1}{seg} \times 4\times 10^{-40} \\
  &\approx 2.4\times 10^{-18}seg^{-1}.
\end{align*}
	
Esto es lo que se conoce como constante de decaimiento $\lambda$ (no se confunda con camino libre medio), la probabilidad de decaimiento por unidad de tiempo, el tiempo de vida media es la inversa de esta constante ($\tau = 1/\lambda$). En este caso $\tau \approx 1.3\times 10^{10}\text{ años}$.
	
\section*{Decaimiento Beta}
	
Aquí hablaremos de partículas y fuerzas que ya conocemos, pero vistas desde el marco de la física nuclear. Como hemos mencionado para núcleos más pesados empiezan a abundar los neutrones, si un núcleo tiene demasiados neutrones extras con respecto a los protones, seguramente se encuentra en un estado menos estable. El núcleo puede convertirse a uno más estable emitiendo un electrón y un anti-neutrino del electrón ($\beta^-$).
	
\begin{equation*}
  {}^AX^Z \rightarrow {}^AY^{Z+1} + e^- +\bar{\nu_e}
\end{equation*}
	
Que realmente el proceso que sucede es el de un neutrón decayendo a un protón
	
\begin{equation*}
  n\rightarrow p + e^- + \bar{\nu_e}
\end{equation*}
	
¿Lo recuerdan? Era un ejemplo de interacción débil pues hay un $W^-$ intermediario. Ya conocemos los principios de este decaimiento y podemos saltarnos una parte de su tratamiento. De igual manera existe otro decaimiento ($\beta^+$)
	
\begin{equation*}
  {}^AX^Z \rightarrow {}^AY^{Z-1} + e^+ +\nu_e,
\end{equation*}
	
\noindent en este caso
	
\begin{equation*}
  p\rightarrow n+e^+ + \nu_e.
\end{equation*}
	
Y también existe la captura electrónica
	
\begin{equation*}
  {}^AX^Z + e^- \rightarrow {}^AY^{Z-1}  +\nu_e,
\end{equation*}
	
\noindent que se traduce a la interacción
	
\begin{equation*}
  p+e^- \rightarrow n + \nu_{e}
\end{equation*}
	
Este último es un fenómeno bastante peculiar donde el núcleo atrapa a un electrón de las órbitas interiores el átomo, los electrones restantes hacen una cascada hacia abajo para llenar el hueco dejado, uno o varios rayos $X$ son liberados.
	
La característica de estos decaimientos es que $\Delta A = 0$ y $|\Delta Z| = 1$.
	
De hacer un análisis parecido al del decaimiento alfa, y si despreciamos al neutrino, notaríamos que la mayor parte de la energía se la llevaría el electrón, pero experimentalmente se ve que el espectro de energías es continuo, de forma que esa energía la comparte con el neutrino.
	
Podemos hacer el análisis de energía tomando en cuenta las tres partículas del lado derecho
	
\begin{align*}
  M_Pc^2 &= T_H + M_Hc^2 + T_{e^-} + m_ec^2 + T_{\bar{\nu}_e} + m_{\bar{\nu}_e}c^2 \\
  T_H + T_{e^-} + T_{\bar{\nu}_e} =& M_Pc^2 - M_Hc^2 - m_ec^2 - m_{\bar{\nu}_e}c^2 
\end{align*}
		
Analizando este término se pueden ver las distintas opciones. Para que la emisión de un electrón suceda debe cumplirse que
	
\begin{align*}
  (M(A,Z)-M(A,Z+1)-m_{\nu_e})c^2 &\geq 0 \\
  \approx (M(A,Z)-M(A,Z+1))c^2 &\geq 0.  
\end{align*}
	
Con la masas $M_P$ y $M_H$ las de los átomos completos, incluyendo electrones. Dado que el núcleo hijo es muy pesado su energía cinética será muy reducida, también se puede omitir, quedando que toda la energía liberada corresponde a las energías cinéticas del electrón y el neutrino, $E\approx T_e+T_{\nu}$.
	
La energía de desintegración para la emisión del positrón
	
\begin{align*}
  E &= (M(A,Z) - M(A,Z-1) - m_e - m_{\nu})c^2 \\
  E &= M(A,Z) - M(A,Z-1) - 2m_e -m_{\nu_e})c^2 \\
  &\approx (M(A,Z) - M(A,Z-1) - 2m_e)c^2
\end{align*}
	
Con las mismas condiciones del caso pasado. Para captura electrónica
	
\begin{align*}
  E &= (M_P + m_e - M_H - m_{\nu})c^2 \\
  E &= (M(A,Z) - M(A,Z-1)  -m_{\nu_e})c^2 \\
  &\approx (M(A,Z) - M(A,Z-1))c^2
\end{align*}
	
En todos estos casos se desprecian las energías de ligadura (en $eV$) de los electrones en el átomo. El espectro de energías nos puede dar una idea de la masa de los neutrinos, pero por su valor tan pequeño no es una tarea fácil.
	
Si regresamos a la consideración del pozo de potencial, sabemos que hay una barrera centrífuga que de igual manera evita que ciertas configuraciones puedan salir del pozo. Si el momento angular orbital de la partícula que sale es $\ell=1$ será más complicado que escape. En cambio si $\ell=0$ será más sencillo. Entonces se dividen los decaimientos:
	
\begin{itemize}
\item $L=0$, decaimiento $\beta$ permitido
\item $L>0$, decaimientos $\beta$ prohibidos ($L=1$ primero prohibido, $L=2$ segundo prohibio, etc.)
\end{itemize}
	
En todo esto hemos considerado la interacción del núcleo con el campo del electrón y el neutrino, a partir de sus valores podemos saber cuanto momento angular es emitido. El momento angular que se llevan es el que corresponde al momento angular orbital $L$ de ambas partículas. Considerando el momento angula inicial y final del núcleo tenemos dos posibilidades
	
\begin{itemize}
\item $J_f = J_i + L$, es una transición de Fermi
\item $J_f = J_i + L + 1$, es una transición de Gamow-Teller
\end{itemize}
	
Graficar las energías de enlace (o los defectos de masa) de isóbaros que decaen por decaimiento beta es útil para ilustrar el proceso, pongamos por ejemplo los isóbaros con $A=76$, que serían ${}^{76}Zn^{30}$, ${}^{76}Ga^{31}$, ${}^{76}Ge^{32}$, ${}^{76}As^{33}$, ${}^{76}Se^{34}$, ${}^{76}Br^{35}$, ${}^{76}Kr^{36}$, ${}^{76}Rb^{37}$ y ${}^{76}Sr^{38}$. En la figura \ref{fig:parabola} se grafican los excesos de masa contra los números atómicos, los núcleos par par van en la parábola de abajo, los impar-impar en la parábola de arriba, la dirección de las flechas indica si es un decaimiento $\beta^+$ o $\beta^-$ y sólo en un caso se puede ver una captura electrónica en la flecha que va del ${}^{76}As^{33}$ al ${}^{76}Ge^{32}$. El resto, si van de izquierda a derecha la $Z$ va aumentando, se emite un $e^-$, son decaimientos $\beta^-$, si las flechas van de derecha a izquierda, la $Z$ disminuye, se emite un $e^+$, el decaimiento es $\beta^+$. 
	
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{beta_parabola2.png}
    \caption{Excesos de masa para los isóbaros con $A= 76$ que tienen decaimientos $\beta$. Imagen adaptada de \cite{Poves} con licencia CC-BY 3.0}
    \label{fig:parabola}
  \end{center}
\end{figure}
	
¿Qué núcleos inestables decaen  por emisión beta, cuáles por alfa? Para verlo tenemos la tabla de los isótopos estables e inestables, como se puede ver en la figura \ref{fig:estabilidad}. En medio se distingue una línea negra de los núcleos estables, en azul todos los núcleos inestables que decaen por emisión $\beta$, en amarillo por emisión $\alpha$. Por razones que veremos en la siguiente sección no están listados los núcleos que decaen por emisión $\gamma$. La línea del goteo del protón se muestra como una línea negra en la parte superior de la franja, más allá de esta línea los núcleos desprenden un protón para poder llegar a la estabilidad.
		
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{estabilidad.png}
    \caption{Tabla de nucleones. Imagen de Hiroyuki Koura en el dominio público}
    \label{fig:estabilidad}
  \end{center}
\end{figure}
	
\section*{Decaimiento Gamma}
	
Cuando un núcleo decae por emisión alfa o beta puede decaer a un estado excitado del núcleo hijo, este núcleo a su vez puede decaer por alguno de los dos procesos o en caso de no desintegrarse puede llegar a su estado base emitiendo un fotón, un $\gamma$. Los espacios entre niveles de energía en el núcleo son de alrededor de $50\ keV$, pero el gama emitido puede deberse al paso del nucleón por más de un nivel, por lo que su energía está en el orden de $MeV$.
El fotón se lleva al menos una unidad de momento angular (el fotón tiene espín de 1 en unidades de $\hbar$) y es un proceso que conserva paridad (a diferencia del decaimiento beta). Imaginemos un núcelo en un estado inicial con energía $E_i$ que pasa por emisión de un fotón de frecuencia $\nu$ a un estado final con energía $E_f$
	 
\begin{equation*}
  h\nu = E_i - E_f
\end{equation*}
	 
El mismo análisis se puede hacer si en lugar de emitir absorbe, sólo con signo cambiado.
	 
Podríamos pensar que la energía del fotón determina unívocamente el espaciamiento entre niveles, pero al emitir el fotón el núcleo también siente una pequeña reacción, entonces una parte de la energía debe irse en momento del núcleo
	 
\begin{equation*}
  \frac{h\nu}{c} = Mv,
\end{equation*}
	 
\noindent con $M$ y $v$ la masa y velocidad del estado final. Agregando esta parte a la conservación de la energía
	 
\begin{align*}
  E_i-E_f =& h\nu + \frac{1}{2}Mv^2 \\
  =& h\nu +\frac{1}{2M}\left( \frac{h\nu}{c} \right)^2 \\
  \text{reacomodando } h\nu =& \left( E_i - E_f - \frac{h^2 \nu^2}{2Mc^2} \right) = E_i - E_f - \Delta E_R, 
\end{align*}
	
\noindent donde $\Delta E_R$ denota la energía cinética de reacción del núcleo. Los estados son denotados por su energía, como lo hemos estado haciendo, en particular todo estado inestable realmente tiene un valor dentro de un rango de energía, un grosor, $\partial E = \Gamma$, de igual forma los estados inestables tienen un tiempo de vida media $\tau$, por el principio de incertidumbre podemos escribir
	
\begin{align*}
  \tau \Gamma &\approx \hbar \\
  \text{o diciéndolo de otra forma } \Gamma &\approx \frac{\hbar}{\tau} \approx \text{incertidumbre en }(E_i-E_f)
\end{align*}
	
El valor exacto del nivel de energía es incierto, la mejor aproximación es dada por el grosor $\Gamma$. Así tenemos que si $\Delta E_R \ll \Gamma$, es decir, que la energía de reacción del núcleo es mucho menor que el nivel de energía (tomando en cuenta su incertidumbre), el proceso de emisión es posible, de lo contrario no habrá decaimineto $\gamma$.
	
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.6\linewidth]{nivelesse.jpg}
    \caption{Niveles de energía para el ${}^{72}Se^{34}$. Tomado de \cite{Krane}}
    \label{fig:niveles}
  \end{center}
\end{figure}
	
Veamos un ejemplo, tomemos el ${}^{50}Ti^{22}$ como el núcleo, sólo por tener un valor intermedio de $A$. Calculamos la masa $M\approx 46512.11\ MeV/c^2$ y consideramos un espaciamiento de $h\nu\gtrsim 100keV = 10^5 eV$
	
\begin{equation}
  \Delta E_R = \frac{(h\nu)^2}{2Mc^2} = \frac{(10^5 eV)^2}{2(46.512\times 10^9 eV)} \approx 0.215\ eV 
  \label{ec:delta}
\end{equation}
	
Ahora consideramos un tiempo de vida media para un nivel de energía alrededor de $10^{-12}seg$, que es un valor que suele tener un estado excitado, podemos aproximar la anchura
	
\begin{equation*}
  \Gamma \approx \frac{\hbar}{\tau} \approx \frac{6.582\times 10^{-22}MeV\cdot seg}{10^{-12}seg} = 6.582\times 10^{-10} MeV = 6.582 \times 10^{-4} eV
\end{equation*}
	
Es claro que $\Gamma \ll \Delta E_R$, por lo que la emisión no puede suceder. Pero hemos tomado los valores correctos ¿qué es lo que pasa? Nuestro problema está en la energía de reacción $\Delta E_R$, que debe ser reducida al mínimo.
	
El efecto Mössbauer explica como es que sucede: considera que los núcleos están fuertemente unidos en una red cristalina, de esta forma cuando se emite un $\gamma$ no es sólo el núcleo el que siente la reacción, si no toda la red, esto multiplica la masa en el denominador de la ecuación \ref{ec:delta} por un factor muy grande, lo que reduce inmensamente la energía de reacción. El efecto Mössbauer explica con gran precisión las energías de los niveles nucleares.
	
Como se muestra en la figura \ref{fig:niveles} los estados excitados se encuentran marcados por energía, de estos niveles excitados puede emitirse un fotón para bajar al estado base.
	
La conversión interna sucede cuando el $\gamma$ saliente del núcleo excita a uno de los electrones de las capas interiores del átomo y lo desprende, en este caso en lugar de detectar un fotón se detecta un electrón de alta energía ¿cómo diferenciarlo de un electrón de decaimiento $\beta^-$? Primero por su energía, puede ser mucho mayor a la de la partícula $\beta$, pero sobre todo por su espectro de energía. Como es arrancado de una capa interior de los niveles atómicos, sólo cierta energía puede desprenderlo, de igual forma su espectro de energía está cuantizado. Mientras que el espectro de energía de los $\beta$ es continuo.
	
\section{Leyes de decaimiento}
	
Ya hablamos de los posibles decaimientos nucleares, lo que falta es ver como describir matemáticamente estos procesos. Ya sea que se emitan $\alpha$'s, $\beta$'s o $\gamma$'s en gran medida lo hemos tratado como un proceso estadístico, esto sucede para un grupo grande de partículas y no tenemos la certeza del momento exacto del decaimiento. Lo que hay es una probabilidad constante de que suceda el decaimiento para cada núcleo, la llamamos $\lambda$. Si $N$ es el número de núcleos radiactivos de un cierto tipo en un tiempo definido y lambda es la probabilidad constante de decaimiento por unidad de tiempo, el cambio en el número de nucleos en un intervalo de tiempo infinitesimal $dt$ es
	
\begin{equation*}
  dN = N(t+dt)- N(t) = -N(t)\lambda dt
\end{equation*}
	
El signo menos se debe a que la cantidad de núcleos del tipo especificado se van reduciendo. Tomemos $N_0 = N(t=0)$, entonces $N(t)$ a cualquier tempo $t$ estará dado por
	
\begin{align*}
  \frac{dN}{N} =& -\lambda dt,\\
  \int_{N_0}^N \frac{dN}{N} =& -\lambda \int_0^t dt, \\
  ln\frac{N(t)}{N_0} =& -\lambda t \\
  N(t) =& N_0 e^{-\lambda t}
\end{align*}
	
El número de núcleos que no han decaido se reduce exponencialmente, llegando a cero en un tiempo infinito.
	
Para establecer una escala de tiempo en la que hablaremos de los decaimientos nucleares definimos el tiempo de vida media como el periodo de tiempo para el cual la mitad de los núcleos muestra decae, lo llamaremos $t_{\frac{1}{2}}$
	
\begin{align*}
  N(t_{\frac{1}{2}}) =& \frac{N_0}{2} = N_0e^{-\lambda t_{\frac{1}{2}}} \\
  \text{de otra forma } \lambda t_{\frac{1}{2}} =& ln2 \\
  \text{entonces } t_{\frac{1}{2}} =& \frac{ln2}{\lambda}
\end{align*}
	
¿Cuál es la relación de $t_{\frac{1}{2}}$ con la $\tau$ que vimos antes? La $\tau$ es la vida mediana que se obtiene como
	
\begin{align*}
  \langle t \rangle = \tau =& \frac{\int_0^{\infty} t N(t) dt}{\int_0^{\infty} N(t) dt} \\
  =& \frac{N_0 \int_0^{\infty} t e^{-\lambda t} dt}{N_0\int_0^{\infty} e^{-\lambda t} dt} \\
  =& \frac{\lambda^{-2}}{\lambda^{-1}} = \frac{1}{\lambda}
\end{align*}
	
De esta forma $t_{\frac{1}{2}} = \tau (ln2)$.
	
El número de desintegraciones por unidad de tiempo, que se llama la actividad de una muestra, se escribe como
	
\begin{equation*}
  \mathcal{A} = | \frac{dN}{dt} | = \lambda N(t) = \lambda N_0 e^{-\lambda t}
\end{equation*}

Si esta actividad la medimos en decaimientos por segundo las unidades usadas son los becquereles, históricamente se utilizo la actividad del ${}^{226}Ra^{88}$, que corresponde a $3.7 \times 10^{10}$ desintegraciones por segundo, es decir $3.7 \times 10^{10}\ Bq$, que corresponde a $1\ Ci$, un curie (nombrado en honor a Pierre). El núcleo radiactivo elegido para definir la unidad era exageradamnte activo, así que por lo regular las muestras tienen una actividad en el orden de $mCi$ (milicuries) o incluso $\mu Ci$ (micro curies). El rutherford es otra unidad, un poco más cercana a los valores promedio de actividad, $1 rd = 10^6 Bq$.
	
En caso de que un núcleo pueda decaer por diversos procesos con probabilidades por unidad de tiempo $\lambda_1$, $\lambda_2$, $\lambda_3$, ..., la probabilida total por unidad de tiempo será
	
\begin{equation*}
  \lambda = \lambda_1 + \lambda_2 + \lambda_3 + ... 
\end{equation*}	
	
De esta forma
	
\begin{equation*}
  \frac{1}{t_{\frac{1}{2}}} = \frac{1}{(t_{\frac{1}{2}})_1} + \frac{1}{t_{(\frac{1}{2}})_2}+ \frac{1}{(t_{\frac{1}{2}})_3} + ... 
\end{equation*}	 
	
En el caso de que un núcleo decaiga a otro hijo, y este a su vez es radiactivo (con una probabilidad constante diferente), etonces tenemos
	
\begin{align*}
  -\frac{dN_1}{dt} &= \lambda_1 N_1 \\
  \frac{dN_2}{dt} &= \lambda_1 N_1 - \lambda_2 N_2
\end{align*}
	
Tenemos un sistema de ecuaciones diferenciales acopladas, tomamos $N_1 = N_{10}$ y $t=0$

\begin{align*}
  N_1 =& N_{10}e^{-\lambda_1 t}\\
  N_2 =& N_{10}\frac{\lambda_1}{\lambda_2 - \lambda_1} (e^{-\lambda_1 t} - e^{-\lambda_2 t})
\end{align*}
	
En esta solución $\lambda_2 \gg \lambda_1$, es decir $(t_{\frac{1}{2}})_2 \ll (t_{\frac{1}{2}})_1$, de lo contrario tenemos un proceso desacoplado en el que aún no termina de decaer el núcleo padre y el núcleo hijo ya está decayendo.
	
Vamos a hacer un ejemplo, tomemos el ${}^{226}Ra^{88}$, del que ya conocemos su actividad de $3.7 \times 10^{10}\ Bq$, tras 500 años ¿cuál será su actividad? Sabemos que la vida media de este núcleo es de $1600$ años, convertido a segundos para tener las unidades necesarias $t_{\frac{1}{2}}=5.04576\times 10^{10}seg.$, los $500$ años se convierten en $1.5768\times 10^{10} seg.$. Entonces tenemos
	
\begin{equation*}
  \mathcal{A}(t=1.5768\times 10^{10}seg.) = \lambda N_0 e^{-\lambda t}
\end{equation*}
	
Pero ¿y la actividad inicial? ¿Dónde la usamos y dónde entra en esa ecuación? La actividad inicial sería $\mathcal{A}(t=0)$
	
\begin{equation*}
  \mathcal{A}(t=0) = \lambda N_0 e^{-\lambda 0} = \lambda N_0 = \mathcal{A}_0
\end{equation*}

Entonces tenemos
	
\begin{equation*}
  \mathcal{A}(t=1.5768\times 10^{10}seg.) = \mathcal{A}_0 e^{-\lambda t}
\end{equation*}

Y ya sólo resta sustituir, recordando que $\lambda = ln2/t_{\frac{1}{2}}$
	
\begin{equation*}
  \mathcal{A}(t=1.5768\times 10^{10}seg.) = (3.7\times 10^{10} Bq) e^{-\frac{ln2}{5.04576\times 10^{10}seg.} (1.5768\times 10^{10} seg.)}
\end{equation*}
	
De aquí $\mathcal{A}(t=1.5768\times 10^{10}seg.) \approx 2.3\times 10^{10}Bq$.
	 		
\begin{thebibliography}{10}
\bibitem{DasFerbel}Das, A., Ferbel, T. ``Introduction to Nuclear and Particle Physics'', Segunda edición, World Scientific Publishing Co., 2003.
  
\bibitem{Krane}Krane, Keneth S. ``Introductory Nuclear Physics'', Segunda edición, John Wiley and Sons, 1988.
		
\bibitem{Cohen}Cohen, Bernard L. ``Concepts of Nuclear Physics'', McGraw-Hill, 1971.
		
\bibitem{Poves}Giuliani, Andrea, Poves, Alfredo. (2012). ``Neutrinoless Double-Beta Decay''. Advances in High Energy Physics. 2012. 10.1155/2012/857016. 		


\end{thebibliography}	
		 				 	 
\end{document}
