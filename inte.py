import numpy as np
from random import random
    

#Definimos la funcion
def f(x):
    return np.sin(1/(x*(2-x)))**2
    
#Definimos las inicializaciones
N= 1000
count = 0

#ciclo principal
for i in range(N):
    x=2*random()
    y= random()
    if y<f(x):
      count+=1
      
#Evaluamos la integral
I=2*count/N
print(I)
